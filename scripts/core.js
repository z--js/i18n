'use strict';

/* global translations, jsyaml */
import { Locale } from './locale.js';
import './js-yaml.min.js';

const langChanger = document.getElementById('langChanger');

langChanger.addEventListener('change', () => { setLang(langChanger.value); });

const translate = (locale) => {
  const tr = [];
  Object.keys(translations).forEach(key => {
    tr[key] = locale.t(translations[key][0], translations[key][1]);
  });

  var el = document.querySelectorAll('[data-locale]');
  for (let i = 0; i < el.length; i += 1) {
    el[i].innerHTML = tr[el[i].getAttribute('data-locale')];
  }
};

const setLang = (newLang) => {
  if (!newLang) {
    newLang = sessionStorage.getItem('lang') || 'ru';
    langChanger.value = newLang;
  }

  sessionStorage.setItem('lang', newLang);
  fetch(`locales/${newLang}.yml`)
    .then(response => response.text())
    .then(
      (data) => {
        const locale = new Locale(jsyaml.load(data));
        translate(locale);
      }
    )
    .catch(error => console.log(error));
};

window.addEventListener('load', () => { setLang(); });
