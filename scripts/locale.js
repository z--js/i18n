'use strict';

import {
  deepFind,
  // deepMerge
} from './paramTools.js';

export class Locale {
  constructor (lang) {
    this.lang = lang;
  }

  t (keyPath, options = {}, debug = true) {

    const interpolate = (template, variables, fallback) => template
      .replace(
        /%{[^{]+}/g,
        match => match.slice(2, -1).trim().split('.').reduce(
          (res, key) => res[key] || fallback, variables
        )
      );

    const template = deepFind(this.lang, keyPath) || (debug ? keyPath : '');

    return interpolate(template, options);
  }
}
